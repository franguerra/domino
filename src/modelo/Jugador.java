package modelo;

import java.util.ArrayList;

public class Jugador{
	
	private ArrayList<Ficha> fichas;
	private int puntos;
	private String nombre;
	
	public Jugador(String nombre) {
		this.nombre = nombre;
		puntos = 0;
		fichas = new ArrayList<Ficha>();
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public int getPuntos() {
		return puntos;
	}
	
	public void agregarPuntos(int puntos) {
		this.puntos += puntos;
	}
	
	public void agregarFicha(Ficha ficha) {
		fichas.add(ficha);
	}
	
	public void eliminarFicha(Ficha ficha) {
		fichas.remove(ficha);
	}

	public Ficha[] getFichas() {
		Ficha[] arrayFichas = new Ficha[fichas.size()];
		return fichas.toArray(arrayFichas);
	}
	
	
	
}
