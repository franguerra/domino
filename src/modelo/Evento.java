package modelo;

public class Evento implements IEvento{
	
	private Eventos tipo;
	private Ficha[] fichas;
	private Ficha ficha;
	private Jugador[] jugadores;
	private Jugador jugador;
	
	public Evento() {;
		
	}
	
	public Evento(Eventos tipo, Jugador jugador) {
		this.tipo = tipo;
		this.jugador = jugador;
	}

	public Eventos getTipo() {
		return tipo;
	}

	public void setTipo(Eventos tipo) {
		this.tipo = tipo;
	}

	public Ficha[] getFichas() {
		return fichas;
	}

	public void setFichas(Ficha[] fichas) {
		this.fichas = fichas;
	}

	public Ficha getFicha() {
		return ficha;
	}

	public void setFicha(Ficha ficha) {
		this.ficha = ficha;
	}

	public Jugador[] getJugadores() {
		return jugadores;
	}

	public void setJugadores(Jugador[] jugadores) {
		this.jugadores = jugadores;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}
	
}
