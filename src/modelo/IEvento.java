package modelo;

public interface IEvento {
	public Eventos getTipo();
	public Ficha[] getFichas();
	public Ficha getFicha();
	public Jugador[] getJugadores();
	public Jugador getJugador();
}