package modelo;

import java.util.ArrayList;

public class Mesa {
	
	private ArrayList<Ficha> fichasMesa; 
	private int extremo1;
	private int extremo2;
	
	
	public Mesa() {
		fichasMesa = new ArrayList<Ficha>();
	}
	
	public void agregarFicha(Ficha ficha, Boolean izquierda) {
		if (fichasMesa.size() == 0) {
			fichasMesa.add(ficha);
			extremo1 = ficha.getNum1();
			extremo2 = ficha.getNum2();
		}
		else {
			if (izquierda) {  
				if (ficha.getNum2() != extremo1)
					ficha.intercambiarNumeros();
				fichasMesa.add(0, ficha);
				extremo1 = ficha.getNum1();
			}
			else {
				if (ficha.getNum1() != extremo2)
					ficha.intercambiarNumeros();
				fichasMesa.add(ficha);
				extremo2 = ficha.getNum2();
			}
		}
	}
	
	public void eliminarFicha(Ficha ficha) {
		fichasMesa.remove(ficha);
	}
	
	public Ficha[] getFichas() {
		Ficha[] fichas = new Ficha[fichasMesa.size()];
		return fichasMesa.toArray(fichas);
	}

	public int getExtremo1() {
		return extremo1;
	}

	public int getExtremo2() {
		return extremo2;
	}
	
	
}



