package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import observer.Observable;
import observer.Observador;

public class PartidaDomino implements Observable {

	private int puntosMaximos;
	private ArrayList<Jugador> jugadores;
	int ronda;
	private Mesa mesa;
	private ArrayList<Ficha> pozo;
	private Evento evento;
	
	private ArrayList<Observador> observadores;
	
	private Jugador jugador;
	private Ficha ficha;
	private boolean extremo;

	
	public PartidaDomino() {
		puntosMaximos = 50;
		jugadores = new ArrayList<Jugador>();
		ronda = 0;
		mesa = new Mesa();
		pozo = new ArrayList<Ficha>();
		evento = new Evento();
		
		observadores = new ArrayList<Observador>();
		
		jugador = null;
		ficha = null;
		extremo = false;
	}
	
	// FUNCIONES QUE EJECUTA EL CONTROLADOR:
	public void establecerPuntos(int puntos) { 
		puntosMaximos = puntos;
	}
	
	public void agregarJugador(String nombre) { 
		jugadores.add(new Jugador(nombre));
	}
	
	public void ponerFichaMesa() {
		jugador.eliminarFicha(ficha);
	}
	
	public void iniciarPartida() {
		
		if (jugadores.size() < 2 || jugadores.size() > 4) { // Compruebo la cantidad de jugadores
			evento = new Evento(Eventos.NUM_JUGADORES_INVALIDOS, jugador);
			notificar(evento);
			
		} else {
			evento = new Evento(Eventos.COMIENZO_PARTIDA, jugador); 
			notificar(evento);
			hacerPozo(); // Hago el pozo
			while (!puntosAlcanzados()) { 
				ronda += 1;   
				rehacerPozo();  // Devuelvo las fichas que tenian los jugadores y las de la mesa al pozo
				repartirFichas();
				evento = new Evento(Eventos.NUEVA_RONDA, jugador);
				notificar(evento);
				ronda(); // ronda
				
				evento = new Evento(Eventos.MOSTRAR_PUNTOS, jugador);  // muestro los puntos de la ronda
				notificar(evento);
			}
			evento = new Evento(Eventos.MOSTRAR_JUGADOR_GANADOR, jugador);  
			evento.setJugador(jugadorGanador());	// muestro al ganador
			notificar(evento);	
		}
	}
	
	private void ronda() {
		
		int jugadoresPasan = 0;	// contador de los jugadores que pasaron, para manejar el "cierre"
		Jugador mano;
		if (ronda == 1) { // En la primer ronda empieza el doble mas alto
			ficha = buscarDobleMasAlto();   // busco el doble mas alto
			jugador = jugadorPropietario(ficha);  	// obtengo el jugador que tiene esa ficha
			mano = jugador;  // mano
			evento = new Evento(Eventos.COMIENZA_FICHA_ALTA, jugador); 
			evento.setFicha(ficha);  
			notificar(evento);  // muestro que comienza la ronda el doble mas alto
			jugador.eliminarFicha(ficha);  // la elimino del jugador
			evento = new Evento(Eventos.MOSTRAR_FICHAS_JUGADOR, jugador); // actualizo las fichas del jugador
			evento.setFichas(jugador.getFichas());	
			notificar(evento); 	
			mesa.agregarFicha(ficha, false); // agrego la ficha a la mesa
			evento = new Evento(Eventos.MOSTRAR_MESA, jugador); // muestro la mesa
			evento.setFichas(mesa.getFichas());
			notificar(evento); 			
		} else {	// sino es la primer ronda entonces el jugador elige la ficha con la que quiere comenzar
			mano = jugador;
			evento = new Evento(Eventos.COMIENZA_FICHA_ELEGIDA, jugador); 
			notificar(evento);
			
			evento = new Evento(Eventos.JUGADOR_ELEGIR_FICHA, jugador);
			evento.setFichas(fichasPuedePoner());
			notificar(evento);
		}
		jugador = siguienteJugador(jugador); // paso al siguiente jugador de la ronda
		
		boolean seguir = true;
		while (seguir) {
			evento = new Evento(Eventos.MOSTRAR_TURNO, jugador); // muestro de quien es el turno
			notificar(evento);
			evento = new Evento(Eventos.MOSTRAR_MESA, jugador); // muestro las fichas de la mesa
			evento.setFichas(mesa.getFichas());
			notificar(evento);  
			evento = new Evento(Eventos.MOSTRAR_FICHAS_JUGADOR, jugador); // muestro las fichas del jugadoes
			evento.setFichas(jugador.getFichas());
			notificar(evento); 
			
			if (fichasPuedePoner().length > 0) {  // SI PUEDE PONER
				jugadoresPasan = 0;  
				evento = new Evento(Eventos.JUGADOR_ELEGIR_FICHA, jugador); // mostrar cuales quiere poner y esperar a que elija
				evento.setFichas(fichasPuedePoner());
				notificar(evento);
				if (jugador.getFichas().length == 0) { // DOMINO, el jugador se quedo sin fichas
					seguir = false;
					sumarPuntosGanador(jugador); // sumo puntos al ganador
					evento = new Evento(Eventos.DOMINO, jugador); // mostrar ronda terminada por DOMINO
					notificar(evento);
				}
				
			} else {  // SI NO PUEDE PONER            
				boolean puedePoner = false;	
				while (pozo.size() > 0 && !puedePoner) {  // mientras que el pozo tenga fichas y no pueda poner:
					evento = new Evento(Eventos.JUGADOR_JUNTAR_POZO, jugador); // muestro menu para juntar ficha del pozo
					notificar(evento);
					if (sePuedeColocar(ficha, mesa.getExtremo1()) || sePuedeColocar(ficha, mesa.getExtremo2())) { // si la ficha se puede colocar:
						jugadoresPasan = 0;	// reinicio el contador de los jugadores que pasan
						puedePoner = true;			
						evento = new Evento(Eventos.JUNTO_PUEDE_TIRAR, jugador); // aviso que puede tirar, pone la ficha en la mesa
						evento.setFicha(ficha);
						notificar(evento);	
					} else { // si la ficha no se puede colocar:
						evento = new Evento(Eventos.JUNTO_NO_PUEDE_TIRAR, jugador); // aviso que no se puede colocar
						evento.setFicha(ficha);
						notificar(evento);
					}
				}
				
				if (!puedePoner) {  // si salio del ciclo de juntar del pozo y no puede poner, entonces se acabaron las fichas del pozo
					jugadoresPasan += 1;  // incremento el contador de los jugadores que pasan
					evento = new Evento(Eventos.JUGADOR_PASA, jugador); // aviso que el jugador pasa
					notificar(evento);
					if (jugadoresPasan == jugadores.size()) { // CIERRE, todos los jugadores pasaron
						seguir = false;
						
						// Busco el jugador que tiene menos puntos de fichas
						int min = 500;
						Jugador aux = jugador;
						for (Jugador j: jugadores) {
							if (obtenerPuntos(j) < min) {
								min = obtenerPuntos(j);
								jugador = j;
							} else if (obtenerPuntos(j) == min) {
								jugador = masCercanoAMano(j, jugador, mano);
							}
						}
						sumarPuntosGanador(jugador);	// sumo puntos al ganador
						evento = new Evento(Eventos.CIERRE, jugador); // aviso que la ronda termino por cierre
						notificar(evento);
						jugador = aux;
					}
				}
			}
			jugador = siguienteJugador(jugador); // siguiente jugador
		}
	}

		
	public int getRonda() { // obtener el numero de ronda
		return ronda;
	}
	
	public Ficha[] fichasPuedePoner() { // obtener las fichas que el jugador puede poner
		Ficha[] fichas;
		ArrayList<Ficha> fichasPuedePoner = new ArrayList<Ficha>();
		if (mesa.getFichas().length == 0) {
			fichas = jugador.getFichas();
		} else {
			for (Ficha f: jugador.getFichas()) {
				if (sePuedeColocar(f, mesa.getExtremo1()) || sePuedeColocar(f, mesa.getExtremo2()) ) 
					fichasPuedePoner.add(f);
			}
			fichas = new Ficha[fichasPuedePoner.size()];
			fichas = fichasPuedePoner.toArray(fichas);
		}
		return fichas;
	}		
	
	public Jugador[] getJugadores() { //obtener los jugadores de la partida
		Jugador[] jugadoresArray = new Jugador[jugadores.size()];
		return jugadores.toArray(jugadoresArray);
	}

	public void ponerFichaMesa(Ficha ficha) { // poner una ficha en la mesa
		jugador.eliminarFicha(ficha);
		if (mesa.getFichas().length == 0) {
			mesa.agregarFicha(ficha, false);
		} else {
			if (sePuedeColocar(ficha, mesa.getExtremo1()) && sePuedeColocar(ficha, mesa.getExtremo2())) {
				evento = new Evento(Eventos.JUGADOR_ELEGIR_EXTREMO, jugador);
				notificar(evento);
			}
			else if (sePuedeColocar(ficha, mesa.getExtremo1())) {
				extremo = true;
			} else 
				extremo = false;
			mesa.agregarFicha(ficha, extremo);
		}
		evento = new Evento(Eventos.MOSTRAR_FICHAS_JUGADOR, jugador);
		evento.setFichas(jugador.getFichas());
		notificar(evento);
		evento = new Evento(Eventos.MOSTRAR_MESA, jugador);
		evento.setFichas(mesa.getFichas());
		notificar(evento);
	}
	
	public void establecerExtremo(boolean extremo) { // establece el extremo donde se tiene que colocar la ficha (cuando puede colocarse en los dos)
		this.extremo = extremo;
	}		
					
	public void juntarFichaPozo() { // junta ficha pozo
		Random random = new Random();
		ficha = pozo.get(random.nextInt(pozo.size())); 
		pozo.remove(ficha);
		jugador.agregarFicha(ficha);
		
		evento = new Evento(Eventos.MOSTRAR_FICHAS_JUGADOR, jugador);
		evento.setFichas(jugador.getFichas());
		notificar(evento);
	}
	////////////////////////////////////////////////////////////	
	// FUNCIONES AUXILIARES:
	
	private void hacerPozo() {  // hago el pozo
		Ficha ficha;
		for(int i=0; i<=6; i+=1) {
			for(int j=i; j<=6; j+=1) {
				ficha = new Ficha(i, j);
				pozo.add(ficha);
			}
		}
		Collections.shuffle(pozo);
	}

	private void rehacerPozo() { // devuelvo fichas al pozo, cuando empieza una nueva ronda
		for (Jugador j: jugadores) {
			for (Ficha f: j.getFichas()) {
				j.eliminarFicha(f);
				pozo.add(f);
			}
		}
		for (Ficha f: mesa.getFichas()) {
			mesa.eliminarFicha(f);
			pozo.add(f);
		}
		Collections.shuffle(pozo);
	}	
		
	private void repartirFichas() { // reparto las fichas a los jugadores
		Random random = new Random();
		for (Jugador j: jugadores) {
			for (int i=1; i<=7; i+=1) {
				Ficha f = pozo.get(random.nextInt(pozo.size()));
				pozo.remove(f);
				j.agregarFicha(f);
			}
		}
	}
	
	private boolean puntosAlcanzados() { // compuerbo si se alcanzaron los puntos necesarios para ganar
		boolean seAlcanzo = false;
		for (Jugador j: jugadores) {
			if (j.getPuntos() >= puntosMaximos)
				seAlcanzo = true;
		}
		return seAlcanzo;
	}

	private Ficha buscarDobleMasAlto() { // busco el doble mas alto entre los jugadores
		Ficha masAlto = null;
		Boolean primerDoble = true;
		for (Jugador j: jugadores) {
			for (Ficha f: j.getFichas()) {
				if (f.esDoble()) {
					if (primerDoble) {
						primerDoble = false;
						masAlto = f;
					} else {
						if (f.getNum1() > masAlto.getNum1()) {
							masAlto = f;
						}
					}
				}
			}
		}
		if (masAlto == null) 
			masAlto = buscarFichaMasAlta();
		return masAlto;
	}
		
	private Ficha buscarFichaMasAlta() { // en caso de no haber dobles, busco la mas alta, si hay mas de una, elijo random
		ArrayList<Ficha> mayores = new ArrayList<Ficha>();
		Ficha masAlta = new Ficha(0, 0);
		for (Jugador j: jugadores) {
			for (Ficha f: j.getFichas()) {
				if (f.valor() > masAlta.valor()) {
					masAlta = f;
					mayores.clear();
					mayores.add(f);
				} else if (f.valor() == masAlta.valor()) {
					mayores.add(f);
				}
			}
		}
		Random random = new Random();
		masAlta = mayores.get(random.nextInt(mayores.size()));
		return masAlta;
	}

	private Jugador jugadorPropietario(Ficha ficha) { // retorno el jugador que tiene la ficha que se pasa por parametro
		Jugador jugador = null;
		for (Jugador j: jugadores) {
			for (Ficha f: j.getFichas()) {
				if (f == ficha) {
					jugador = j;
				}
			}
		}
		return jugador;
	}
	
	private Jugador siguienteJugador(Jugador jugadorActual) { // retorno el siguiente jugador
		int indice = jugadores.indexOf(jugadorActual) + 1;
		if (indice == jugadores.size())
			indice = 0;
		return jugadores.get(indice);
	}
	
	private boolean sePuedeColocar(Ficha ficha, int extremo) { // se pasa una ficha y un extremo del domino, comprueba si se puede colocar
		return (ficha.getNum1() == extremo || ficha.getNum2() == extremo);
	}

	private int obtenerPuntos(Jugador jugador) { // obtiene los puntos de las fichas del jugador
		int suma = 0;
		for (Ficha f: jugador.getFichas()) {
			suma += f.valor();
		}
		return suma;
	}
	
	private void sumarPuntosGanador(Jugador jugadorGanador) { // suma puntos de las fichas de los otros jugadores al jugador
		for (Jugador j: jugadores) {
			if (j != jugadorGanador) 
				jugadorGanador.agregarPuntos(obtenerPuntos(j));
		}
	}
	
	private Jugador masCercanoAMano(Jugador j1, Jugador j2, Jugador mano) { // retorna el jugador mas cercano a la mano
		int distanciaJ1 = 0;
		int distanciaJ2 = 0;
		Jugador aux = mano;
		while (aux != j1) {
			aux = siguienteJugador(mano);
			distanciaJ1 += 1;
		}
		aux = mano;
		while (aux != j2) {
			aux = siguienteJugador(mano);
			distanciaJ2 += 1;
		}
		if (distanciaJ1 < distanciaJ2)
			aux = j1;
		else
			aux = j2;
		return aux;
	}
		
	private Jugador jugadorGanador() { // retorna el jugador ganador
		Jugador jugador = null;
		for (Jugador j: jugadores) {
			if (j.getPuntos() >= puntosMaximos)
				jugador = j;
		}
		return jugador;
	}
	
	// METODOS DEL OBSERVADOR
	@Override
	public void agregarObservador(Observador observador) {
		observadores.add(observador);
	}
	
	@Override
	public void notificar(Object evento) {
		for (Observador observador: observadores) {
			observador.actualizar(evento);
		}
	}
}