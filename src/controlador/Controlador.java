package controlador;

import modelo.PartidaDomino;
import modelo.Ficha;
import modelo.IEvento;
import modelo.Jugador;
import observer.Observador;
import vista.VistaConsola;

public class Controlador implements Observador {

	private PartidaDomino modelo;
	private VistaConsola vista;
	
	
	public Controlador(PartidaDomino modelo) {
		this.modelo = modelo;
		modelo.agregarObservador(this);
	}
	
	public void setVista(VistaConsola vista) {
		this.vista = vista;
	}
	
	@Override
	public void actualizar(Object evento) {
		if (evento instanceof IEvento) {
			IEvento ievento = (IEvento) evento;
			switch(ievento.getTipo()) {
			
			case NUM_JUGADORES_INVALIDOS:
				vista.numeroJugadoresInvalido();
				break;
				
			case COMIENZO_PARTIDA:
				vista.inicioPartida();
				break;
				
			case NUEVA_RONDA:
				vista.mostrarNumeroRonda(modelo.getRonda());
				break;	
				
			case COMIENZA_FICHA_ALTA:
				vista.comienzaFichaAlta(ievento.getFicha(), ievento.getJugador());
				break;
			
			case COMIENZA_FICHA_ELEGIDA:
				vista.comienzaFichaElegida(ievento.getJugador());
				break;
				
			case MOSTRAR_FICHAS_JUGADOR:
				vista.mostrarFichasJugador(ievento.getFichas(), ievento.getJugador());
				break;	
				
			case MOSTRAR_MESA:
				vista.mostrarFichasMesa(ievento.getFichas());
				break;
				
			case JUGADOR_ELEGIR_FICHA:
				vista.mostrarMenuElegirFicha(ievento.getFichas());	
				break;	
			
			case MOSTRAR_TURNO:
				vista.mostrarTurno(ievento.getJugador());
				break;
				
			case JUGADOR_JUNTAR_POZO:
				vista.mostrarMenuJuntarPozo();
				break;	
				
			case JUNTO_PUEDE_TIRAR:
				vista.juntoPuedeTirar(ievento.getFicha());
				break;	
				
			case JUNTO_NO_PUEDE_TIRAR:
				vista.juntoNoPuedeTirar(ievento.getFicha());
				break;	
				
			case JUGADOR_ELEGIR_EXTREMO:
				vista.mostrarMenuElegirExtremo();
				break;	
			
			case JUGADOR_PASA:
				vista.jugadorPasa(ievento.getJugador());
				break;
				
			case DOMINO:
				vista.mostrarDomino(ievento.getJugador());
				break;
				
			case CIERRE:
				vista.mostrarCierre(ievento.getJugador());
				break;
				
			case MOSTRAR_PUNTOS:
				vista.mostrarPuntosJugadores();
				break;
				
			case MOSTRAR_JUGADOR_GANADOR:
				vista.mostrarJugadorGanador(ievento.getJugador());
				break;
			}
 		}
	}

	public void establecerPuntos(int puntos) {
		modelo.establecerPuntos(puntos);
	}
	
	public void agregarJugador(String nombre) {
		modelo.agregarJugador(nombre);
	}
	
	public Jugador[] getJugadores() {
		return modelo.getJugadores();
	}
	
	public void iniciarJuego() {
		modelo.iniciarPartida();
	}
	
	public void ponerFichaMesaIndice(int numFicha) {
		Ficha ficha = modelo.fichasPuedePoner()[numFicha-1];
		modelo.ponerFichaMesa(ficha);
	}
	
	public void ponerFichaMesa(Ficha ficha) {
		modelo.ponerFichaMesa(ficha);
	}
	
	public void juntarFichaPozo() {
		modelo.juntarFichaPozo();
	}
	
	public void establecerExtremo(int opcion) {
		modelo.establecerExtremo((opcion == 1));
	}
}
