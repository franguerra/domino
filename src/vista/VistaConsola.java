package vista;

import java.util.Scanner;

import controlador.Controlador;
import modelo.Ficha;
import modelo.Jugador;

public class VistaConsola {
	
	private Scanner input;
	
	private Controlador controlador;
	
	public VistaConsola(Controlador controlador) {
		this.controlador = controlador;
		input = new Scanner(System.in);
		controlador.setVista(this);
		
	}
	
	public void iniciar() {
		System.out.println("-------------------------------------------------------");
		System.out.println("------------------------ DOMINO -----------------------");
		System.out.println("-------------------------------------------------------");
		System.out.println();
		int opcion = -1;
		while (opcion != 0) {
			opcion = mostrarMenuPrincipal();
			switch (opcion) {
			case 1:
				establecerPuntos();
				break;
			case 2:
				agregarJugador();
				break;
			case 3:
				mostrarJugadores();
				break;
			case 4:
				iniciarJuego();
				break;
			}
		}
	}

	private int mostrarMenuPrincipal() {
		int opcion = -1;
		while (opcion > 4 || opcion < 0) {	
			System.out.println("-------------- OPCIONES DE CONFIGURACION --------------");
			System.out.println("1 - Establecer cantidad de puntos necesarios para ganar");
			System.out.println("2 - Agregar jugador");
			System.out.println("3 - Mostrar jugadores");
			System.out.println("4 - Iniciar partida");
			System.out.println("0 - Salir del juego");
			System.out.println();
			System.out.print("Ingrese una opcion: ");
			opcion = input.nextInt();
			System.out.println();
		}
		return opcion;
	}
	
	private void establecerPuntos() {
		int puntos = -1;
		while (puntos < 50 || puntos > 300) {
			System.out.println("----------------- ESTABLECIENDO PUNTOS ----------------");
			System.out.println("Los puntos deben ser entre 50 y 300");
			System.out.print("Ingrese los puntos que son necesarios para ganar: ");
			puntos = input.nextInt();
			System.out.println();
		}
		controlador.establecerPuntos(puntos);
	}
	
	private void agregarJugador() {
		input.nextLine();
		String nombre = "";
		while (nombre.equals("")) {
			System.out.println("------------------ AGREGANDO JUGADOR ------------------");
			System.out.print("Ingrese nombre: ");
			nombre = input.nextLine();
		}
		System.out.println("Jugador agregado");
		System.out.println();
		
		controlador.agregarJugador(nombre);
	}
	
	private void mostrarJugadores() {
		Jugador[] jugadores = controlador.getJugadores();
		System.out.println("----------------- MOSTRANDO JUGADORES -----------------");
		for (Jugador jugador: jugadores) {
			System.out.println(jugador.getNombre());
		}
		System.out.println();
	}
	
	private void iniciarJuego() {
		controlador.iniciarJuego();
	}
	
	
	public void numeroJugadoresInvalido() {
		System.out.println("Para jugar se necesitan al menos dos jugadores");
		System.out.println();
	}
	
	
	public void inicioPartida() {
		System.out.println("LA PARTIDA COMIENZA");
		System.out.println();
	}
	
	
	public void mostrarNumeroRonda(int numRonda) {
		System.out.println("Comienza la ronda " + numRonda);
		System.out.println();
	}
	
	
	public void comienzaFichaAlta(Ficha ficha, Jugador jugador) {
		System.out.println("Comienza " + jugador.getNombre() + ", porque tiene el doble mas alto: [" + ficha.getNum1() + "|" + ficha.getNum2() + "]");
		System.out.println();
	}
	
	
	public void comienzaFichaElegida(Jugador jugador) {
		System.out.println("Comienza " + jugador.getNombre() + ", elije la ficha con la que quiere comenzar");
		System.out.println();
	}
	
	
	public void mostrarFichasJugador(Ficha[] fichas, Jugador jugador) {
		System.out.println("Fichas de " + jugador.getNombre() + ":");
		for (Ficha ficha: fichas) {
			System.out.print(" [" + ficha.getNum1() + "|" + ficha.getNum2() + "]");
		}
		System.out.println();
		System.out.println();
	}
	
	
	public void mostrarFichasMesa(Ficha[] fichas) {
		System.out.println("Fichas en mesa:");
		for (Ficha ficha: fichas) {
			System.out.print(" [" + ficha.getNum1() + "|" + ficha.getNum2() + "]");
		}
		System.out.println();
		System.out.println();
	}
	
	
	public void mostrarMenuElegirFicha(Ficha[] fichas) {
		int opcion = 1;
		System.out.println("Fichas que podes poner:");
		for (Ficha ficha: fichas) {
			System.out.println(opcion +  "- [" + ficha.getNum1() + "|" + ficha.getNum2() + "]");
			opcion += 1;
		}
		System.out.println();
		opcion = 0;
		while (opcion < 1 || opcion > fichas.length) {
			System.out.print("Elegi la ficha que queres poner: ");
			opcion = input.nextInt();
		}
		System.out.println();
		controlador.ponerFichaMesaIndice(opcion);
	}
	
	
	public void mostrarTurno(Jugador jugador) {
		System.out.println("Es el turno de " + jugador.getNombre());
		System.out.println();
	}
	
	
	public void mostrarMenuJuntarPozo() {
		System.out.println("No podes poner fichas, tenes que juntar del pozo");
		int opcion = 0;
		while (opcion != 1) {
			System.out.print("Ingresa 1 para juntar del pozo: ");
			opcion = input.nextInt();
		}
		System.out.println();
		controlador.juntarFichaPozo();
	}
	
	
	public void juntoPuedeTirar(Ficha ficha) {
			System.out.println("La ficha que juntaste es [" + ficha.getNum1() + "|" + ficha.getNum2() + "]");
			System.out.println("Se puede colocar");
			System.out.println();
			int opcion = 0;
			while (opcion != 1) {
				System.out.print("Ingresa 1 para colocar la ficha: ");
				opcion = input.nextInt();
			}
			System.out.println();
			controlador.ponerFichaMesa(ficha);
		}
	
	
	public void juntoNoPuedeTirar(Ficha ficha) {
		System.out.println("La ficha que juntaste es [" + ficha.getNum1() + "|" + ficha.getNum2() + "]");
		System.out.println("No se puede colocar");
		System.out.println();
	}
	
	
	public void mostrarMenuElegirExtremo() {
		System.out.println("La ficha se puede poner en ambos extremos:");
		int opcion = 0;
		while (opcion < 1 || opcion > 2) {
			System.out.println("1 - Izquierda");
			System.out.println("2 - Derecha");
			System.out.println();
			System.out.print("Ingrese la opcion: ");
			opcion = input.nextInt();
		}
		System.out.println();
		controlador.establecerExtremo(opcion);
	}
	
	
	public void jugadorPasa(Jugador jugador) {
		System.out.println(jugador.getNombre() + " pasa porque el pozo se acabo y no puede tirar");
		System.out.println();
	}
	
	
	public void mostrarDomino(Jugador jugador) {
		System.out.println(jugador.getNombre() + " gano por DOMINO");
		System.out.println();
	}
	
	public void mostrarCierre(Jugador jugador) {
		System.out.println(jugador.getNombre() + " gano por CIERRE");
		System.out.println();
	}
	
	
	public void mostrarPuntosJugadores() {
		System.out.println("Puntajes:");
		Jugador[] jugadores = controlador.getJugadores();
		for (Jugador jugador: jugadores) {
			System.out.println(jugador.getNombre() + " tiene " + jugador.getPuntos() + " puntos");
		}
		System.out.println();
	}
	
	
	public void mostrarJugadorGanador(Jugador jugador) {
		System.out.println(jugador.getNombre() + " es el ganador");
		System.out.println();
	}
}
