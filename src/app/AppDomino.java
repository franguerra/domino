package app;

import controlador.Controlador;
import modelo.PartidaDomino;
import vista.VistaConsola;

public class AppDomino {

	public static void main(String[] args) {
		PartidaDomino modelo = new PartidaDomino();
		Controlador controlador = new Controlador(modelo);
		VistaConsola vista = new VistaConsola(controlador);
		vista.iniciar();
	}

}
