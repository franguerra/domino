package observer;

public interface Observador {
	
	public void actualizar(Object evento); // Actualiza las vistas
	
}

